ad_page_contract {
    @author Neophytos Demetriou
} {
    {task_id:integer,multiple,notnull ""}
    {project_id:integer ""}
    {return_url:trim,notnull ""}
}

# auto assign project manager to each of the tasks and approve
set current_user_id [auth::get_user_id]

set project_manager_p 1
if { $project_id ne {} } {
    set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id"]
    switch $project_type_id {
        100 - 101 {
            set project_id [db_string parent "select parent_id from im_projects where project_id = :project_id"]
        }
    }
    set project_manager_p [im_biz_object_admin_p $current_user_id $project_id]
}

array set attributes [list confirm_hours_are_the_logged_hours_ok_p "t"]
set the_action "finish"
array set assignments [list]
set msg "Approved Hours"

foreach one_task_id $task_id {
    
    # Assign project_managers if not already assigned
    # And no other project manager is available
    if {$project_manager_p} {
        # Count how many are (P)
        set num_of_other_pms [db_string num_of_pms "
                        select count(*)
                        from acs_rels r,
                        im_biz_object_members m
                        where r.object_id_one=:project_id
                        and r.rel_id = m.rel_id
                        and m.object_role_id = 1301"]

        if {$num_of_other_pms eq 0} {
	    # No other pm available, assign to current pm
            db_exec_plsql assign_to_user \
                "select im_workflow__assign_to_user(:one_task_id,:current_user_id)"
        }
    }

    # Only run this for assigned users
    array set task_info [wf_task_info $one_task_id]
    if {$task_info(this_user_is_assigned_p)} {
        callback workflow_task_before_update -task_id $one_task_id -action $the_action -msg $msg -attributes [array get attributes]
        set journal_id [wf_task_action -user_id $current_user_id -msg $msg -attributes [array get attributes] -assignments [array get assignments] $one_task_id $the_action]
        callback workflow_task_after_update -task_id $one_task_id -action $the_action -msg $msg -attributes [array get attributes]
    }
}

wf_sweep_message_transition_tcl

ad_returnredirect $return_url
