ad_page_contract {
    @author Neophytos Demetriou
} {
    {task_id:integer,multiple,notnull ""}
    {project_id:integer ""}
    {return_url:trim,notnull ""}
}

# auto assign project manager to each of the tasks and approve
set current_user_id [auth::get_user_id]

set project_manager_p 1
if { $project_id ne {} } {
    set project_manager_p [im_biz_object_admin_p $current_user_id $project_id]
}

array set attributes [list confirm_hours_are_the_logged_hours_ok_p "f"]
set the_action "finish"
array set assignments [list]
set msg "Approved Hours"
if {$project_manager_p } { 
    foreach one_task_id $task_id {

        db_exec_plsql assign_to_user \
            "select im_workflow__assign_to_user(:one_task_id,:current_user_id)"

        callback workflow_task_before_update -task_id $one_task_id -action $the_action -msg $msg -attributes [array get attributes]
        set journal_id [wf_task_action -user_id $current_user_id -msg $msg -attributes [array get attributes] -assignments [array get assignments] $one_task_id $the_action]
        callback workflow_task_after_update -task_id $one_task_id -action $the_action -msg $msg -attributes [array get attributes]

    }
}

wf_sweep_message_transition_tcl

ad_returnredirect $return_url

